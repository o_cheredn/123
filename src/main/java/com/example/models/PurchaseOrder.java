package com.example.models;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by Oleksandr on 2/20/2017.
 */
@Entity
@Data
public class PurchaseOrder {

    @Id @GeneratedValue
    Long id;


    LocalDate issueDate;
    LocalDate paymentSchedule;

    @Column(precision=8,scale=2)
    BigDecimal total;

    @Enumerated(EnumType.STRING)
    POStatus status;

    /*LocalDate startDate;
    LocalDate endDate;*/

    @Embedded
    BusinessPeriod rentalPeriod;

    @OneToMany
    List<PlantReservation> reservations;
    /* this is the example, but should we specify  @ManyToOne ????? */

    @ManyToOne
    PlantInventoryEntry plant;


}
