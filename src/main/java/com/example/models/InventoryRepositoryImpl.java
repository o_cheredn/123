package com.example.models;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by lgarcia on 2/17/2017.
 */
public class InventoryRepositoryImpl implements CustomInventoryRepository {
    @Autowired
    EntityManager em;

    @Override
    public List<PlantInventoryEntryAvailability> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        TypedQuery query = em.createQuery(
                "select new com.example.models.PlantInventoryEntryAvailability" +
                        "(i.plantInfo.id, i.plantInfo.name, i.plantInfo.description, i.plantInfo.price, count(i)) " +
                        "from PlantInventoryItem i " +
                        "where lower(i.plantInfo.name) like ?1 " +
                        "and i.equipmentCondition = 'SERVICEABLE' " +
                        "and not exists " +
                        "(select r.plant.id from PlantReservation r " +
                        "where r.plant.id = i.id and " +
                        "(?2 between r.period.startDate and r.period.endDate " +
                        "or ?3 between r.period.startDate and r.period.endDate " +
                        "or (?2 < r.period.startDate and ?3 > r.period.endDate))) " +
                        "group by i.plantInfo.id order by i.plantInfo.id asc"
                , PlantInventoryEntryAvailability.class)
                .setParameter(1, "%"+name+"%")
                .setParameter(2, startDate)
                .setParameter(3, endDate);
        return query.getResultList();
    }
    @Override
    public List<PlantInventoryEntry> findAvailable(String name, LocalDate startDate, LocalDate endDate) {
        return em.createQuery("select e from PlantInventoryEntry e where lower(e.name) like ?1", PlantInventoryEntry.class)
                .setParameter(1, "%"+name+"%")
                .getResultList();
    }

    public boolean findIfAvailableInPeriodStrict(long id, LocalDate startDate, LocalDate endDate){

        if (startDate.compareTo(endDate) > 0){
            return false;
        }
        else{
            TypedQuery query = em.createQuery(
                    "select count(i) from PlantInventoryItem i " +
                            "where i.plantInfo.id = ?1 " +
                            "and i.equipmentCondition = 'SERVICEABLE' " +
                            "and (exists " +
                            "(select r from PlantReservation r " +
                            "where r.plant.id = i.id and " +
                            "not(r.period.startDate <= ?2 and r.period.endDate >= ?3)) " +
                            "or not exists (select r from PlantReservation r where r.plant.id = i.id))"
                    , Number.class)
                    .setParameter(1, id)
                    .setParameter(2, endDate)
                    .setParameter(3, startDate);

            long numberOfItems = ((Number) query.getSingleResult()).longValue();
            boolean result = false;

            if (numberOfItems != 0) {
                result = true;
            }

            return result;
        }

    }

    public boolean findIfAvailableInPeriodRelaxedWithInitial(long id, LocalDate startDate, LocalDate endDate, LocalDate initialDate){
        if (startDate.compareTo(endDate) > 0){
            return false;
        }
        else{
            TypedQuery query = em.createQuery(
                    "select count(i) from PlantInventoryItem i " +
                            "where i.plantInfo.id = ?1 " +
                            "and i.equipmentCondition = 'SERVICEABLE' " +
                            "and (exists " +
                            "(select r from PlantReservation r " +
                            "where r.plant.id = i.id and " +
                            "not(r.period.startDate <= ?2 and r.period.endDate >= ?3)) " +
                            "or not exists (select r from PlantReservation r where r.plant.id = i.id) " +
                            "or exists (select r from PlantReservation r where r.plant.id = i.id and ?4 >= r.period.startDate and ?5 >= r.period.endDate)" +
                            ")"
                    , Number.class)
                    .setParameter(1, id)
                    .setParameter(2, endDate)
                    .setParameter(3, startDate)
                    .setParameter(4, initialDate.plusWeeks(3))
                    .setParameter(5, startDate.minusWeeks(1));

            long numberOfItems = ((Number) query.getSingleResult()).longValue();
            boolean result = false;

            if (numberOfItems != 0) {
                result = true;
            }

            return result;
        }
    }

    public boolean findIfAvailableInPeriodRelaxed(long id, LocalDate startDate, LocalDate endDate){
        return findIfAvailableInPeriodRelaxedWithInitial(id, startDate, endDate, LocalDate.now());
    }

    @Override
    public List<PlantInventoryItem> findNotHiredPlants() {
        return em.createQuery("select i from PlantInventoryItem i "+
                " where not exists " +
                "(select r.plant.id from PlantReservation r " +
                "where r.plant.id = i.id and " +
                "(r.period.startDate between ?1 and ?2))"
                , PlantInventoryItem.class)
                .setParameter(1, LocalDate.now().minusMonths(6))
                .setParameter(2, LocalDate.now())
                .getResultList();

    }

}
