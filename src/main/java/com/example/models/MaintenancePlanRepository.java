package com.example.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Oleksandr on 2/20/2017.
 */
@Repository
public interface MaintenancePlanRepository  extends JpaRepository<MaintenancePlan, Long>{
/* */
}
