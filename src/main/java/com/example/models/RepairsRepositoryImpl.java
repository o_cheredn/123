package com.example.models;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.List;
/**
 * Created by Oleksandr on 2/22/2017.
 */
public class RepairsRepositoryImpl implements CustomRepairsRepository {

    @Autowired
    EntityManager em;

    @Override
    public List<MaintenancePlan> findCount() {
        //return em.createNativeQuery("SELECT COUNT(ID),YEAR_OF_ACTION FROM MAINTENANCE_PLAN GROUP BY YEAR_OF_ACTION")
        return em.createNativeQuery("SELECT COUNT(MAINTENANCE_PLAN.ID) AS NUMBER_OF_REPAIRS, MAINTENANCE_PLAN.YEAR_OF_ACTION AS YEAR\n" +
                "FROM MAINTENANCE_PLAN_TASK\n" +
                "INNER JOIN MAINTENANCE_PLAN\n" +
                "        ON MAINTENANCE_PLAN_TASK.MAINTENANCE_PLAN_ID = MAINTENANCE_PLAN.ID\n" +
                "INNER JOIN MAINTENANCE_TASK \n" +
                "        ON MAINTENANCE_PLAN_TASK.TASK_ID = MAINTENANCE_TASK.ID\n" +
                "WHERE MAINTENANCE_TASK.TYPE_OF_WORK = 'CORRECTIVE' AND MAINTENANCE_PLAN.YEAR_OF_ACTION BETWEEN 2011 AND 2016\n" +
                "GROUP BY MAINTENANCE_PLAN.YEAR_OF_ACTION ")
                //.setParameter(1, "%"+name+"%")
                .getResultList();
    }


    @Override
    public List<MaintenancePlan> findCost() {
        return em.createNativeQuery("SELECT SUM(MAINTENANCE_TASK.PRICE) AS NUMBER_OF_REPAIRS, MAINTENANCE_PLAN.YEAR_OF_ACTION AS YEAR\n" +
                "FROM MAINTENANCE_PLAN_TASK\n" +
                "INNER JOIN MAINTENANCE_PLAN\n" +
                "        ON MAINTENANCE_PLAN_TASK.MAINTENANCE_PLAN_ID = MAINTENANCE_PLAN.ID\n" +
                "INNER JOIN MAINTENANCE_TASK \n" +
                "        ON MAINTENANCE_PLAN_TASK.TASK_ID = MAINTENANCE_TASK.ID\n" +
                "WHERE MAINTENANCE_TASK.TYPE_OF_WORK = 'CORRECTIVE' AND MAINTENANCE_PLAN.YEAR_OF_ACTION BETWEEN 2011 AND 2016\n" +
                "GROUP BY MAINTENANCE_PLAN.YEAR_OF_ACTION")
                //.setParameter(1, "%"+name+"%")
                .getResultList();
    }
}
