package com.example.models;

/**
 * Created by Oleksandr on 2/20/2017.
 */


import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
@Entity
@Data

public class MaintenanceTask {

    @Id @GeneratedValue
    Long id;

    String description;

    @Enumerated(EnumType.STRING)
    TypeOfWork type_of_work;

    @Column(precision=8,scale=2)
    BigDecimal price;

   // @Embedded
    //BusinessPeriod rentalPeriod;

    @ManyToOne
    PlantReservation reservation;

}
