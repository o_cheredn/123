package com.example.models;

/**
 * Created by Oleksandr on 2/20/2017.
 */
public enum POStatus {
    PENDING, OPEN, REJECTED, CLOSED
}
