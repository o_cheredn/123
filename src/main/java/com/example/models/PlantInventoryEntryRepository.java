package com.example.models;

import org.hibernate.mapping.List;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.thymeleaf.expression.Lists;

/**
 * Created by lgarcia on 2/10/2017.
 */
@Repository
public interface PlantInventoryEntryRepository extends JpaRepository<PlantInventoryEntry, Long> {

    java.util.List<PlantInventoryEntry> findByNameContaining(String str);

    @Query("select p from PlantInventoryEntry p where LOWER(p.name) like ?1")
    java.util.List<PlantInventoryEntry> finderMethod(String name);

    @Query(value="select * from plant_inventory_entry where LOWER(name) like ?1", nativeQuery=true)
    java.util.List<PlantInventoryEntry> finderMethodV2(String name);

}
