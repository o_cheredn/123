package com.example.models;

import java.math.BigDecimal;

/**
 * Created by gkgranada on 23/02/2017.
 */
public class PlantInventoryEntryAvailability {
    public long id;
    public String name;
    public String description;
    public BigDecimal price;
    public long serviceableItems;

    public PlantInventoryEntryAvailability(long id, String name, String description, BigDecimal price, long serviceableItems) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.serviceableItems = serviceableItems;
    }
}
