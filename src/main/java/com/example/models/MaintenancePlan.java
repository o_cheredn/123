package com.example.models;

import lombok.Data;

import javax.annotation.Generated;
import javax.persistence.*;
import java.util.List;

/**
 * Created by Oleksandr on 2/20/2017.
 */
@Entity
@Data

public class MaintenancePlan {
    @Id @GeneratedValue
    Long id;

    Integer year_of_action;

    @Embedded
    BusinessPeriod period;

    @ManyToOne
    PlantInventoryItem plant;

    @OneToMany(cascade={CascadeType.ALL})
    List<MaintenanceTask> task;
}
