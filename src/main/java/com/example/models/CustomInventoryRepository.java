package com.example.models;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by lgarcia on 2/17/2017.
 */
public interface CustomInventoryRepository {
    List<PlantInventoryEntry> findAvailable(String name, LocalDate startDate, LocalDate endDate);
    boolean findIfAvailableInPeriodStrict(long id, LocalDate startDate, LocalDate endDate);
    boolean findIfAvailableInPeriodRelaxed(long id, LocalDate startDate, LocalDate endDate);
    boolean findIfAvailableInPeriodRelaxedWithInitial(long id, LocalDate startDate, LocalDate endDate, LocalDate initialDate);
    List<PlantInventoryEntryAvailability> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate);
    List<PlantInventoryItem> findNotHiredPlants();
}
