package com.example.models;

import java.util.List;

/**
 * Created by Oleksandr on 2/22/2017.
 */
public interface CustomRepairsRepository {
    List<MaintenancePlan> findCount();
    List<MaintenancePlan> findCost();
}
