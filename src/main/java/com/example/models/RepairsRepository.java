package com.example.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Oleksandr on 2/23/2017.
 */
@Repository
public interface RepairsRepository extends JpaRepository<MaintenancePlan, Long>, CustomRepairsRepository{
}
