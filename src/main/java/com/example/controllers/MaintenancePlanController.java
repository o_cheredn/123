package com.example.controllers;

import com.example.models.MaintenancePlan;
import com.example.models.RepairsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class MaintenancePlanController {
    @Autowired
    RepairsRepository repo;

    @GetMapping("/repnumber")
    public List<MaintenancePlan> findCount() {
        return repo.findCount();
    }



    @GetMapping("/repcost")
    public List<MaintenancePlan> findCost() {
        return repo.findCost();
    }
}
