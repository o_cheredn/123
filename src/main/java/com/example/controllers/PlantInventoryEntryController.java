package com.example.controllers;

import com.example.models.*;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by lgarcia on 2/10/2017.
 */
@RestController
public class PlantInventoryEntryController {
    @Autowired
    InventoryRepository repo;

    /*@GetMapping("/plants")
    public List<PlantInventoryItem> findAll() {
        return repo.findAvailablePlants("exc", LocalDate.now(), LocalDate.now());
    }*/

    @GetMapping("/plants/notHired")
    public List<PlantInventoryItem> findNotHiredPlants() {
        return repo.findNotHiredPlants();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/plants/{plantName}/{start}/{end}")
    public List<PlantInventoryEntryAvailability> findAvailablePlants(
            @PathVariable String plantName, @PathVariable String start, @PathVariable String end) {
        LocalDate startDate = LocalDate.parse(start);
        LocalDate endDate = LocalDate.parse(end);
        return repo.findAvailablePlants(plantName, startDate, endDate);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/plants/{id}/{start}/{end}/strict")
    public boolean findIfAvailableInPeriodStrict(@PathVariable String id, @PathVariable String start, @PathVariable String end){
        LocalDate startDate = LocalDate.parse(start);
        LocalDate endDate = LocalDate.parse(end);
        long longId = Long.parseLong(id);
        return repo.findIfAvailableInPeriodStrict(longId, startDate, endDate);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/plants/{id}/{start}/{end}/relaxed")
    public boolean findIfAvailableInPeriodRelaxed(@PathVariable String id, @PathVariable String start, @PathVariable String end){
        LocalDate startDate = LocalDate.parse(start);
        LocalDate endDate = LocalDate.parse(end);
        long longId = Long.parseLong(id);
        return repo.findIfAvailableInPeriodRelaxed(longId, startDate, endDate);
    }
}
